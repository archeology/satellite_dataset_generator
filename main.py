import os
import multiprocessing

from src.generator import FrameGenerator


def start_process(random_seed=5252):
    generate_count =  os.getenv('GENERATED_COUNT') // os.getenv('WORKERS') 
    generator = FrameGenerator(fell_obj_path=os.getenv('FELL_OBJ_PATH'),
                               cloud_obj_path=os.getenv('CLOUD_OBJ_PATH'),
                               back_layer_dir=os.getenv('BACK_LAYER_DIR'),
                               save_dir=os.getenv('SAVE_DIR'),
                               generate_count=generate_count,
                               random_seed=random_seed)
    generator.generate_frames()

if __name__ == "__main__":
    list_of_processes = []
    for idx in range(os.getenv('WORKERS')):
        process = multiprocessing.Process(target=start_process, args=(idx,), daemon=True)
        list_of_processes.append(process)
    for process in list_of_processes:
        process.start()
    for process in list_of_processes:
        process.join()