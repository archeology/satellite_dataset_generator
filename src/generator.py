import os
import uuid
import random

import rasterio
import numpy as np
import geopandas as gpd
from tqdm import tqdm, trange
from rasterio.windows import Window
from shapely.geometry import Polygon, MultiPolygon
from src.mask_drawer import draw_mask


class FrameGenerator:
    def __init__(self, fell_obj_path: str, cloud_obj_path: str, 
                 back_layer_dir: str, save_dir:str, 
                 generate_count: int, random_seed=5252) -> None:
        
        random.seed = random_seed
        self.fell_obj_df = gpd.read_file(fell_obj_path)
        self.cloud_obj_df = gpd.read_file(cloud_obj_path)
        self.frame_size = os.getenv('FRAME_SIZE')
        self.generate_count = generate_count
        
        self.frame_save_path = os.path.join(save_dir, 'images')
        os.makedirs( self.frame_save_path, exist_ok=True)
        self.mask_save_path = os.path.join(save_dir, 'labels')
        os.makedirs(self.mask_save_path, exist_ok=True)
        
        back_names = [name for name in os.listdir(back_layer_dir) if name.endswith('.jp2')]
        self.back_layer_paths = [os.path.join(back_layer_dir, name) for name in back_names]
        
        self.used_back_layer_path = None
        self.back_layer_profile = None
        self.back_layer = None
        self.back_layer_mask = None
        
    def generate_frames(self):
        for _ in trange(self.generate_count):
            self._generate_frame()
    
    def _generate_frame(self):
        self.used_back_layer_path = random.choice(self.back_layer_paths)
        self._load_back_layer()
        fell_obj_sample = self.fell_obj_df.sample(
            n=random.randint(os.getenv('MIN_FELL_ON_FRAME'), os.getenv('MAX_FELL_ON_FRAME'))
        )
        front_layers_data = self._get_front_layers(fell_obj_sample)
        self._put_on_back(front_layers_data)
        
        if random.uniform(0,1) <= os.getenv(['ADD_CLOUD_PROB']):   
            cloud_obj_sample = self.cloud_obj_df.sample(n=1)
            front_layers_data = self._get_front_layers(cloud_obj_sample)
            self._put_on_back(front_layers_data)      
            
    def _load_back_layer(self):
        src = rasterio.open(self.used_back_layer_path)
        x_len = src.width
        y_len = src.height
        x_offset = random.randint(0, x_len - self.frame_size - 1)
        y_offset = random.randint(0, y_len - self.frame_size - 1)
        
        window = Window(col_off=x_offset, row_off=y_offset,
                        width=self.frame_size, height=self.frame_size)
        self.back_layer =  src.read(window = window, boundless=True)
        self.back_layer_profile = src.profile
        self.back_layer_profile.update({
            'QUALITY': '100',  # Отвечает за сохранение без потери данных
            'REVERSIBLE': 'YES',  # Отвечает за сохранение без потери данных
            'YCBCR420': 'NO',  # Отвечает за сохранение без потери данных
            'nodata': 0,
            'transform': src.window_transform(window),
            'width': self.frame_size,
            'height': self.frame_size
        })
        self.back_layer_mask = np.zeros((1, self.frame_size, self.frame_size))

    def _get_front_layers(self, df_sample): 
        for data_row in df_sample.iterrows():
            geometry = data_row['geometry']
            front_layer = rasterio.open(data_row['fell_path']).read()
            
            if isinstance(geometry, MultiPolygon):
                draw_geometry = [*geometry.geoms]
            else:
                draw_geometry = [geometry]
                
            front_mask = draw_mask(front_layer.shape, draw_geometry)
            front_mask = np.concatenate([front_mask for _ 
                                         in range(front_layer.shape[0])], axis = 0)
            yield  geometry, front_layer, front_mask
    
    def _put_on_back(self, front_layers_data):
        for front_layer_data in front_layers_data:
            geometry, front_layer, front_mask = front_layer_data
            min_x, min_y, max_x, max_y = geometry.bounds
            x_len = max_x - min_x
            y_len = max_y - min_y
            
            x_offset = random.randint(0, self.frame_size - x_len - 1)
            y_offset = random.randint(0, self.frame_size - y_len - 1)

            x, y = geometry.exterior.coords.xy
            geometry = [[(x - min_x + x_offset, y - min_y + y_offset) for x, y in zip(x, y)]]
            
            back_input_mask = draw_mask(self.back_layer.shape, geometry)
            back_input_mask = np.concatenate([back_input_mask for _ 
                                              in range(self.back_layer.shape[0])], axis = 0)
            
            self.back_layer[back_input_mask == 1] = front_layer[front_mask == 1]
            self.back_layer_mask[back_input_mask[:1, ...] == 1] = back_input_mask[:1, ...][back_input_mask[:1, ...] == 1]

    def _save_frame(self):
        profile = self.back_layer_profile
        file_name = f'generated_{uuid.uuid4().hex}.jp2'

        profile.update({
            'count': self.back_layer.shape[0],
        })      
        
        with rasterio.open(os.path.join(self.frame_save_path, file_name), 
                           mode='w', **profile) as dst:
            dst.write(self.back_layer)
            
        profile.update({
            'count': self.back_layer_mask.shape[0]
        })      
        
        with rasterio.open(os.path.join(self.mask_save_path, file_name), 
                           mode='w', **profile) as dst:
            dst.write(self.back_layer_mask)