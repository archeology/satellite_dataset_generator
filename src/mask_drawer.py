from typing import Tuple, Iterable, Union

import numpy as np
import shapely
from PIL import Image, ImageDraw


def draw_mask(shape: Tuple[int, ...], 
              polygons: Iterable[Union[shapely.Geometry, list]]) -> np.ndarray:
    mask = np.zeros(shape[1:])
    mask = Image.fromarray(mask, 'L')
    draw = ImageDraw.Draw(mask)
    for polygon in polygons:
        if not isinstance(polygon, list):
            x, y = polygon.exterior.coords.xy
            polygon =  [(x, y) for x, y in zip(x, y)]
        if len(polygon.geom) < 3:
            continue
        draw.polygon(polygon.geom, outline=1, fill=1)
    return np.expand_dims(np.array(mask), axis=0) 
 